import java.util.regex.Pattern;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.FileInputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

@RunWith(Parameterized.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AdmissaoGlieseXMotion {
  private WebDriver driver;
  private String baseUrl;
  private String baseUrlMotion;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();
  private DSL dsl;
  private AdmissaoGlieseXMotionPage page;
  
  @Parameter
  public String User;
  @Parameter(value=1)
  public String Passwd;
  @Parameter(value=2)
  public String DtNasc;
  @Parameter(value=3)
  public String CIP;
  @Parameter(value=4)
  public String Medico;
  @Parameter(value=5)
  public String Convenio;
  @Parameter(value=6)
  public String Associado;
  @Parameter(value=7)
  public String DtSolicitacao;
  @Parameter(value=8)
  public String ValCartao;
  @Parameter(value=9)
  public String ValPedido;
  @Parameter(value=10)
  public String Medicamento;    
  @Parameter(value=11)
  public String NumChamada;    
  @Parameter(value=12)
  public String ObsTecnicas;    
  @Parameter(value=13)
  public String ObsAtd;
  @Parameter(value=14)
  public String IndClinica;
  //usu�rio e senha Motion
  @Parameter(value=15)
  public String usuarioMotion;
  @Parameter(value=16)
  public String senhaMotion;
  
  /**
   * This function will take screenshot
   * @param webdriver
   * @param fileWithPath
   * @throws Exception
   */
  public static void takeSnapShot(WebDriver webdriver,String fileWithPath) throws Exception{
      //Convert web driver object to TakeScreenshot
      TakesScreenshot scrShot =((TakesScreenshot)webdriver);
      //Call getScreenshotAs method to create image file
      File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
      //Move image file to new destination
      File DestFile=new File(fileWithPath);
      //Copy file at destination
      FileUtils.copyFile(SrcFile, DestFile);
	  
  }
    
  @Before
  public void setUp() throws Exception {
    FirefoxOptions options = new FirefoxOptions();
    options.addArguments("--headless");
    //windows exec
    //System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "\\Utils\\Automation\\geckodriver\\geckodriver.exe");
    //linux exec  
    System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "//Utils//Automation//geckodriver//geckodriver");
    //Caso deseje rodar em modo nao headless, apenas retire o options do driver = new FirefoxDriver(); 
    driver = new FirefoxDriver(options);
    //driver = new FirefoxDriver();
    Thread.sleep(5000);
	//Runtime.getRuntime().exec("C:\\Users\\Herbert\\eclipse-workspace\\GlisexMotion\\gxm_automation\\Utils\\Automation\\authproxy\\HandleAuthentication.exe");
    baseUrl = "http://10.120.42.1:7003/gliese-gxm-hml/loginPortalLSF.jsp";
    baseUrlMotion = "http://10.122.50.46:8001/home.do";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);  
    dsl = new DSL(driver);
    page = new AdmissaoGlieseXMotionPage(driver);
  }
  
  @After
  public void finaliza() {
      //driver.close();
      driver.quit();
  }
  
  @Parameters
  public static Collection<Object[]> getCollection(){
      
      DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");      
      Date date = new Date();
      String date1 = dateFormat.format(date);
	  
      LerArquivo usuarioMotion = new LerArquivo();
      
      //System.out.println(usuarioMotion.getUsuario());
      //System.out.println(usuarioMotion.getSenha());
      
      return Arrays.asList(new Object[][] {
          {
              "MASTER",
              "DEV",
              "06012000",
              "7657779814",
              "52",
              "Lv-amillv",
              "222222222",
              date1,
              "01012020",
              "01012020",
              "Medicamento",
              "NumChamada",
              "ObsTecnicas",
              "ObsAtd",
              "IndClinica",
              usuarioMotion.getUsuario(),
              usuarioMotion.getSenha(),			  
          },
      });
  }
  
  //@Ignore
  @Test
  public void AdmissaoGlieseXMotion_01_Lock() throws Exception {
	  System.out.println("Iniciando Execu��o AdmissaoGlieseXMotion_01_Lock");
	  
      driver.get(baseUrl);
      driver.manage().window().maximize();
      driver.findElement(By.id("username")).click();
      driver.findElement(By.id("username")).clear();
      page.setUsuario(User);
      driver.findElement(By.name("password")).clear();
      page.setSenha(Passwd);
      driver.findElement(By.id("linkEntrar")).click();
      Thread.sleep(5000);
      
      driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Selecione a Empresa'])[1]/following::div[4]")).click();
      driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Selecione a Empresa'])[1]/following::a[13]")).click();
      driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Selecione a Empresa'])[1]/following::a[13]")).click();
      driver.findElement(By.id("resCpUnidade")).click();
      
      new Select(driver.findElement(By.id("resCpUnidade"))).selectByVisibleText("LVI - Vila Matilde");
      driver.findElement(By.xpath("/html/body/div[7]/div/div/div[2]/h2[1]")).click();
      driver.findElement(By.id("btEscolherEmpresa")).click();

      Actions action = new Actions(driver);
      WebElement we = driver.findElement(By.cssSelector("#idMenu > li:nth-child(3) > a:nth-child(1)"));
      action.moveToElement(we).build().perform();
      driver.findElement(By.xpath("//*[@id=\"mnuAtdAdm\"]")).click();

      Actions action1 = new Actions(driver);
      WebElement we1 = driver.findElement(By.cssSelector("#idMenu > li:nth-child(14) > a:nth-child(1)"));
      action1.moveToElement(we1).build().perform();
      Thread.sleep(5000);
      driver.findElement(By.linkText("Gerenciador")).click();
      Thread.sleep(5000);
      driver.findElement(By.id("menuLock")).click();
      Thread.sleep(10000);
      
      try
      {
        WebElement button=driver.findElement(By.xpath("//*[@id=\"iframeAdmissao\"]"));
        driver.switchTo().frame(button);
      }
        catch (NoSuchFrameException e)
      {
        System.out.println(e.getMessage());
      }
      
      Thread.sleep(5000);
      driver.findElement(By.id("lockBtLimpa")).click();
      driver.findElement(By.id("lockBtCIP")).click();
      page.setCIPLock(CIP);
      driver.findElement(By.id("lockBtPesquisar")).click();
      Thread.sleep(5000);
      
      try {
          WebDriverWait wait = new WebDriverWait(driver, 2);
          wait.until(ExpectedConditions.alertIsPresent());
          Alert alert = driver.switchTo().alert();
          alert.accept();
      } catch (Exception e) {
          Thread.sleep(3000);
          driver.findElement(By.xpath("/html/body/div[2]/div/table/tfoot/tr/td/a")).click();
          Thread.sleep(3000);
          Alert alertAccept = driver.switchTo().alert();
          Assert.assertEquals("Confirma a exclus�o de todos os registros apresentados?", alertAccept.getText());
          Thread.sleep(3000);
          driver.switchTo().alert().accept();
          Thread.sleep(3000);
      }
      //this.takeSnapShot(driver, "C:\\Users\\Herbert\\eclipse-workspace\\GlisexMotion\\ScreenShot\\Lock_Exclude.png");
      //driver.close();
	  
	  System.out.println("Finalizando Execu��o AdmissaoGlieseXMotion_01_Lock");	  
  }
  
  //@Ignore ("Test is ignored as a demonstration")
  @Test
  public void AdmissaoGlieseXMotion_05_Ok() throws Exception {
	System.out.println("Iniciando Execu��o AdmissaoGlieseXMotion_05_Ok");	  	  
    driver.get(baseUrl);
    driver.manage().window().maximize();
    driver.findElement(By.id("username")).click();
    driver.findElement(By.id("username")).clear();
    page.setUsuario(User);
    driver.findElement(By.name("password")).clear();
    page.setSenha(Passwd);
    driver.findElement(By.id("linkEntrar")).click();
    Thread.sleep(5000);
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Selecione a Empresa'])[1]/following::div[4]")).click();
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Selecione a Empresa'])[1]/following::a[13]")).click();
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Selecione a Empresa'])[1]/following::a[13]")).click();
    driver.findElement(By.id("resCpUnidade")).click();
    new Select(driver.findElement(By.id("resCpUnidade"))).selectByVisibleText("LVI - Vila Matilde");
    driver.findElement(By.xpath("/html/body/div[7]/div/div/div[2]/h2[1]")).click();
    driver.findElement(By.id("btEscolherEmpresa")).click();
    
    Actions action = new Actions(driver);
    WebElement we = driver.findElement(By.cssSelector("#idMenu > li:nth-child(3) > a:nth-child(1)"));
    action.moveToElement(we).build().perform();
    driver.findElement(By.xpath("//*[@id=\"mnuAtdAdm\"]")).click();
    
    Actions action1 = new Actions(driver);
    WebElement we1 = driver.findElement(By.cssSelector("#idMenu > li:nth-child(14) > a:nth-child(1)"));
    action1.moveToElement(we1).build().perform();

    Thread.sleep(5000);
    
    driver.findElement(By.linkText("Cadastro")).click();
    
    Thread.sleep(10000);
 
    try
    {
      WebElement button=driver.findElement(By.xpath("//*[@id=\"iframeAdmissao\"]"));
      driver.switchTo().frame(button);
    }
      catch (NoSuchFrameException e)
    {
      System.out.println(e.getMessage());
    }
    
    page.setDtNasc(DtNasc);
    page.setCIP(CIP);
    driver.findElement(By.id("lnkVisita")).click();
    Thread.sleep(10000);
    driver.findElement(By.id("Visita_botNova")).click();
    Thread.sleep(5000);
    driver.findElement(By.id("lnkMedico")).click();
    Thread.sleep(10000);
    page.setCRM(Medico);
    Thread.sleep(5000);
    driver.findElement(By.id("lnkConvenio")).click();
    Thread.sleep(10000);
    page.setConvenio(Convenio);
    driver.findElement(By.id("Convenio_valida")).click();
    Thread.sleep(10000);
    page.setMatricula(Associado);
    
    page.setDtSolicitacao(DtSolicitacao);
    driver.findElement(By.id("Convenio_planoCv")).click();
    Select planoconv = new Select (driver.findElement(By.id("Convenio_planoCv")));
    planoconv.selectByVisibleText("103 - Amil Global Jr");
    page.setValCartao(ValCartao);
    page.setValPedido(ValPedido);
    driver.findElement(By.id("lnkExames")).click();
    Thread.sleep(5000);        
    driver.findElement(By.id("Exame_idMnmMat")).click();
    driver.findElement(By.id("Exame_idMnmMat")).clear();
    driver.findElement(By.id("Exame_idMnmMat")).sendKeys("SA");
    driver.findElement(By.id("Exame_idMnmExame")).click();
    driver.findElement(By.id("Exame_idMnmExame")).clear();
    driver.findElement(By.id("Exame_idMnmExame")).sendKeys("GLI");
    driver.findElement(By.id("Exame_valida")).click();
    driver.findElement(By.id("Exame_idDayLabSim")).click();
    driver.findElement(By.id("Exame_salva")).click();
    
    //Confirmar com PO se este exame fara parte do MVP
    /*Thread.sleep(5000);        
    driver.findElement(By.id("Exame_botNovo")).click();
    driver.findElement(By.id("Exame_idMnmMat")).click();
    driver.findElement(By.id("Exame_idMnmMat")).clear();
    driver.findElement(By.id("Exame_idMnmMat")).sendKeys("SA");
    driver.findElement(By.id("Exame_idMnmExame")).click();
    driver.findElement(By.id("Exame_idMnmExame")).clear();
    driver.findElement(By.id("Exame_idMnmExame")).sendKeys("HCVA");
    driver.findElement(By.id("Exame_valida")).click();
    driver.findElement(By.id("Exame_salva")).click();*/
        
    //Confirmar com PO se este exame fara parte do MVP
    /*Thread.sleep(5000);        
    driver.findElement(By.id("Exame_botNovo")).click();
    driver.findElement(By.id("Exame_idMnmMat")).click();
    driver.findElement(By.id("Exame_idMnmMat")).clear();
    driver.findElement(By.id("Exame_idMnmMat")).sendKeys("SA");
    driver.findElement(By.id("Exame_idMnmExame")).click();
    driver.findElement(By.id("Exame_idMnmExame")).clear();
    driver.findElement(By.id("Exame_idMnmExame")).sendKeys("T4T");
    driver.findElement(By.id("Exame_valida")).click();
    Thread.sleep(5000);
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='TUSS'])[1]/following::img[1]")).click();
    driver.findElement(By.id("Exame_salva")).click();*/
    
    Thread.sleep(5000);        
    driver.findElement(By.id("Exame_botNovo")).click();
    driver.findElement(By.id("Exame_idMnmMat")).click();
    driver.findElement(By.id("Exame_idMnmMat")).clear();
    driver.findElement(By.id("Exame_idMnmMat")).sendKeys("SA");
    driver.findElement(By.id("Exame_idMnmExame")).click();
    driver.findElement(By.id("Exame_idMnmExame")).clear();
    driver.findElement(By.id("Exame_idMnmExame")).sendKeys("T4L");
    driver.findElement(By.id("Exame_valida")).click();
    Thread.sleep(5000);  
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='TUSS'])[1]/following::img[1]")).click();
    driver.findElement(By.id("Exame_salva")).click();
    
    Thread.sleep(5000);        
    driver.findElement(By.id("Exame_botNovo")).click();
    driver.findElement(By.id("Exame_idMnmMat")).click();
    driver.findElement(By.id("Exame_idMnmMat")).clear();
    driver.findElement(By.id("Exame_idMnmMat")).sendKeys("SA");
    driver.findElement(By.id("Exame_idMnmExame")).click();
    driver.findElement(By.id("Exame_idMnmExame")).clear();
    driver.findElement(By.id("Exame_idMnmExame")).sendKeys("TSH");
    driver.findElement(By.id("Exame_valida")).click();
    driver.findElement(By.id("Exame_salva")).click();
    
    driver.findElement(By.id("lnkDiversos")).click();
    Thread.sleep(10000);    
    driver.findElement(By.id("Diversos_medicamentos")).click();
    driver.findElement(By.id("Diversos_medicamentos")).clear();
    page.setValDivMedicamentos(Medicamento);
    driver.findElement(By.id("Diversos_numeroChamada")).click();
    driver.findElement(By.id("Diversos_numeroChamada")).clear();
    page.setValNumChamada(NumChamada);
    driver.findElement(By.id("Diversos_observacoes_tecnicas")).click();
    driver.findElement(By.id("Diversos_observacoes_tecnicas")).clear();
    page.setObsTecnicas(ObsTecnicas);
    driver.findElement(By.id("Diversos_observacoes_atendimento")).clear();
    page.setObsAtd(ObsAtd);
    driver.findElement(By.id("Diversos_indicacaoClinica")).click();
    driver.findElement(By.id("Diversos_indicacaoClinica")).clear();
    page.setIndClinica(IndClinica);
    Thread.sleep(3000);
    driver.findElement(By.id("idTrouxeExAntN")).click();
    driver.findElement(By.id("Diversos_prioridade")).click();
    Select prioridade = new Select (driver.findElement(By.id("Diversos_prioridade")));
    prioridade.selectByVisibleText("N�o");
    driver.findElement(By.id("Diversos_ultimaRefeicao")).click();
    Select refeicao = new Select (driver.findElement(By.id("Diversos_ultimaRefeicao")));
    refeicao.selectByVisibleText("Desconhecido");
    
    driver.findElement(By.id("lnkRecipientes")).click();
    String VerRecExames = driver.findElement(By.xpath("//*[@id=\"tabRec\"]/tbody/tr[2]/td[6]")).getText();
    String VerNumRecExames = driver.findElement(By.xpath("//*[@id=\"tabRec\"]/tbody/tr[2]/td[1]")).getText();
    //Assert.assertEquals(true, VerRecExames.contains("DA-T4LSA"));
    //Assert.assertEquals(true, VerRecExames.contains("DA-TSHSA"));
    //Assert.assertEquals(true, VerRecExames.contains("DA-GLISA"));
    //Assert.assertEquals(true, VerNumRecExames.startsWith("5"));
    Thread.sleep(10000);
    //this.takeSnapShot(driver, "C:\\Users\\Herbert\\eclipse-workspace\\GlisexMotion\\ScreenShot\\Rec_K2_GeneratedSuccessfully.png");
    
    driver.findElement(By.id("lnkConclusao")).click();
    Thread.sleep(10000);    
    driver.findElement(By.id("Conclusao_botConfirma")).click();
    Thread.sleep(10000);
    Alert alertAccept = driver.switchTo().alert();
    Assert.assertEquals("Confirma Admissao", alertAccept.getText());
    driver.switchTo().alert().accept();    
    Thread.sleep(10000);
    Alert alert = driver.switchTo().alert();
    Assert.assertEquals("---Existem Restri��es Vindas do Autorizador de Conv�nios---\n" + 
            "\n" + 
            "Erro no Processo de Valida��o de Conv�nios\n" + 
            "\n" + 
            "Numero municipio Fiscal Obrigatorio.\n" + 
            "\n" + 
            "FINALIZA A ADMISS�O MESMO ASSIM?", alert.getText());
    driver.switchTo().alert().accept();    
    Thread.sleep(10000);    
    
    String mainWindow=driver.getWindowHandle();
    Set<String> set =driver.getWindowHandles();
    Iterator<String> itr= set.iterator();
    while(itr.hasNext()){
    String childWindow=itr.next();
        if(!mainWindow.equals(childWindow)){
            driver.switchTo().window(childWindow);
            System.out.println(driver.switchTo().window(childWindow).getTitle());
            driver.close();
        }
    }
    driver.switchTo().window(mainWindow);
    //driver.close();
	
	System.out.println("Finalizando Execu��o AdmissaoGlieseXMotion_05_Ok");	  	  	
	
  }

  //@Ignore
  @Test
  public void AdmissaoGlieseXMotion_02_GLI() throws Exception {
	  
	  System.out.println("Iniciando Execu��o AdmissaoGlieseXMotion_02_GLI");	  	  		  
	  
      driver.get(baseUrl);
      driver.manage().window().maximize();
      driver.findElement(By.id("username")).click();
      driver.findElement(By.id("username")).clear();
      page.setUsuario(User);
      driver.findElement(By.name("password")).clear();
      page.setSenha(Passwd);
      driver.findElement(By.id("linkEntrar")).click();
      Thread.sleep(5000);
      driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Selecione a Empresa'])[1]/following::div[4]")).click();
      driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Selecione a Empresa'])[1]/following::a[13]")).click();
      driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Selecione a Empresa'])[1]/following::a[13]")).click();
      driver.findElement(By.id("resCpUnidade")).click();
      new Select(driver.findElement(By.id("resCpUnidade"))).selectByVisibleText("LVI - Vila Matilde");
      driver.findElement(By.xpath("/html/body/div[7]/div/div/div[2]/h2[1]")).click();
      driver.findElement(By.id("btEscolherEmpresa")).click();
      
      Actions action = new Actions(driver);
      WebElement we = driver.findElement(By.cssSelector("#idMenu > li:nth-child(3) > a:nth-child(1)"));
      action.moveToElement(we).build().perform();
      driver.findElement(By.xpath("//*[@id=\"mnuAtdAdm\"]")).click();
      
      Actions action1 = new Actions(driver);
      WebElement we1 = driver.findElement(By.cssSelector("#idMenu > li:nth-child(14) > a:nth-child(1)"));
      action1.moveToElement(we1).build().perform();

      Thread.sleep(5000);
      
      driver.findElement(By.linkText("Cadastro")).click();
      
      Thread.sleep(10000);
   
      try
      {
        WebElement button=driver.findElement(By.xpath("//*[@id=\"iframeAdmissao\"]"));
        driver.switchTo().frame(button);
      }
        catch (NoSuchFrameException e)
      {
        System.out.println(e.getMessage());
      }
      
      page.setDtNasc(DtNasc);
      page.setCIP(CIP);
    
      driver.findElement(By.id("lnkVisita")).click();
      Thread.sleep(10000);
      driver.findElement(By.id("Visita_botNova")).click();
      
      driver.findElement(By.id("lnkMedico")).click();
      Thread.sleep(5000);
      page.setCRM(Medico);
      
      driver.findElement(By.id("lnkConvenio")).click();
      Thread.sleep(5000);
      page.setConvenio(Convenio);
      driver.findElement(By.id("Convenio_valida")).click();
      Thread.sleep(10000);
      page.setMatricula(Associado);
     
      page.setDtSolicitacao(DtSolicitacao);
      driver.findElement(By.id("Convenio_planoCv")).click();
      Select planoconv = new Select (driver.findElement(By.id("Convenio_planoCv")));
      planoconv.selectByVisibleText("103 - Amil Global Jr");
      page.setValCartao(ValCartao);
      page.setValPedido(ValPedido);
      
      driver.findElement(By.id("lnkExames")).click();
      Thread.sleep(5000);        
      driver.findElement(By.id("Exame_idMnmMat")).click();
      driver.findElement(By.id("Exame_idMnmMat")).clear();
      driver.findElement(By.id("Exame_idMnmMat")).sendKeys("SA");
      driver.findElement(By.id("Exame_idMnmExame")).click();
      driver.findElement(By.id("Exame_idMnmExame")).clear();
      driver.findElement(By.id("Exame_idMnmExame")).sendKeys("GLI");
      driver.findElement(By.id("Exame_valida")).click();
      driver.findElement(By.id("Exame_salva")).click();
            
      driver.findElement(By.id("lnkDiversos")).click();
      Thread.sleep(10000);    
      driver.findElement(By.id("Diversos_medicamentos")).click();
      driver.findElement(By.id("Diversos_medicamentos")).clear();
      page.setValDivMedicamentos(Medicamento);
      driver.findElement(By.id("Diversos_numeroChamada")).click();
      driver.findElement(By.id("Diversos_numeroChamada")).clear();
      page.setValNumChamada(NumChamada);
      driver.findElement(By.id("Diversos_observacoes_tecnicas")).click();
      driver.findElement(By.id("Diversos_observacoes_tecnicas")).clear();
      page.setObsTecnicas(ObsTecnicas);
      driver.findElement(By.id("Diversos_observacoes_atendimento")).clear();
      page.setObsAtd(ObsAtd);
      driver.findElement(By.id("Diversos_indicacaoClinica")).click();
      driver.findElement(By.id("Diversos_indicacaoClinica")).clear();
      page.setIndClinica(IndClinica);
      Thread.sleep(3000);
      driver.findElement(By.id("idTrouxeExAntN")).click();
      driver.findElement(By.id("Diversos_prioridade")).click();
      Select prioridade = new Select (driver.findElement(By.id("Diversos_prioridade")));
      prioridade.selectByVisibleText("N�o");
      driver.findElement(By.id("Diversos_ultimaRefeicao")).click();
      Select refeicao = new Select (driver.findElement(By.id("Diversos_ultimaRefeicao")));
      refeicao.selectByVisibleText("Desconhecido");
      Thread.sleep(10000);
      driver.findElement(By.id("lnkRecipientes")).click();
      Thread.sleep(10000);    
      String VerRecExames = driver.findElement(By.xpath("//*[@id=\"tabRec\"]/tbody/tr[2]/td[6]")).getText();
      String VerNumRecExames = driver.findElement(By.xpath("//*[@id=\"tabRec\"]/tbody/tr[2]/td[1]")).getText();
      Assert.assertEquals(true, VerRecExames.contains("DA-GLISA"));
      Assert.assertEquals(true, VerNumRecExames.startsWith("5"));
      
      driver.findElement(By.id("lnkConclusao")).click();
      Thread.sleep(10000);    
      driver.findElement(By.id("Conclusao_botConfirma")).click();
      Thread.sleep(10000);
      driver.switchTo().alert().accept();
      Thread.sleep(20000);
      driver.switchTo().alert().accept();
      Thread.sleep(10000);
      
      String mainWindow=driver.getWindowHandle();
      Set<String> set =driver.getWindowHandles();
      Iterator<String> itr= set.iterator();
      while(itr.hasNext()){
      String childWindow=itr.next();
          if(!mainWindow.equals(childWindow)){
              driver.switchTo().window(childWindow);
              System.out.println(driver.switchTo().window(childWindow).getTitle());
              driver.close();
          }
      }
      driver.switchTo().window(mainWindow);
      //driver.close();
	  
	  System.out.println("Finalizando Execu��o AdmissaoGlieseXMotion_02_GLI");	  	  		  	  
      
  }

  //@Ignore
  @Test
  public void AdmissaoGlieseXMotion_03_TSH() throws Exception {
	  
	  System.out.println("Iniciando Execu��o AdmissaoGlieseXMotion_03_TSH");	  	  		  	  	  
	  
      driver.get(baseUrl);
      driver.manage().window().maximize();
      driver.findElement(By.id("username")).click();
      driver.findElement(By.id("username")).clear();
      page.setUsuario(User);
      driver.findElement(By.name("password")).clear();
      page.setSenha(Passwd);
      driver.findElement(By.id("linkEntrar")).click();
      Thread.sleep(5000);
      driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Selecione a Empresa'])[1]/following::div[4]")).click();
      driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Selecione a Empresa'])[1]/following::a[13]")).click();
      driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Selecione a Empresa'])[1]/following::a[13]")).click();
      driver.findElement(By.id("resCpUnidade")).click();
      new Select(driver.findElement(By.id("resCpUnidade"))).selectByVisibleText("LVI - Vila Matilde");
      driver.findElement(By.xpath("/html/body/div[7]/div/div/div[2]/h2[1]")).click();
      driver.findElement(By.id("btEscolherEmpresa")).click();
      
      Actions action = new Actions(driver);
      WebElement we = driver.findElement(By.cssSelector("#idMenu > li:nth-child(3) > a:nth-child(1)"));
      action.moveToElement(we).build().perform();
      driver.findElement(By.xpath("//*[@id=\"mnuAtdAdm\"]")).click();
      
      Actions action1 = new Actions(driver);
      WebElement we1 = driver.findElement(By.cssSelector("#idMenu > li:nth-child(14) > a:nth-child(1)"));
      action1.moveToElement(we1).build().perform();

      Thread.sleep(5000);
      
      driver.findElement(By.linkText("Cadastro")).click();
      
      Thread.sleep(10000);
   
      try
      {
        WebElement button=driver.findElement(By.xpath("//*[@id=\"iframeAdmissao\"]"));
        driver.switchTo().frame(button);
      }
        catch (NoSuchFrameException e)
      {
        System.out.println(e.getMessage());
      }
      
      page.setDtNasc(DtNasc);
      page.setCIP(CIP);
      driver.findElement(By.id("lnkVisita")).click();
      Thread.sleep(10000);
      driver.findElement(By.id("Visita_botNova")).click();
      
      driver.findElement(By.id("lnkMedico")).click();
      Thread.sleep(5000);
      page.setCRM(Medico);
      Thread.sleep(5000);
      
      driver.findElement(By.id("lnkConvenio")).click();
      Thread.sleep(5000);
      page.setConvenio(Convenio);
      driver.findElement(By.id("Convenio_valida")).click();
      Thread.sleep(10000);
      page.setMatricula(Associado);
      
      page.setDtSolicitacao(DtSolicitacao);
      driver.findElement(By.id("Convenio_planoCv")).click();
      Select planoconv = new Select (driver.findElement(By.id("Convenio_planoCv")));
      planoconv.selectByVisibleText("103 - Amil Global Jr");
      page.setValCartao(ValCartao);
      page.setValPedido(ValPedido);

      driver.findElement(By.id("lnkExames")).click();
      Thread.sleep(5000);        
      driver.findElement(By.id("Exame_botNovo")).click();
	  Thread.sleep(5000);
      driver.findElement(By.id("Exame_idMnmMat")).click();
      driver.findElement(By.id("Exame_idMnmMat")).clear();
      driver.findElement(By.id("Exame_idMnmMat")).sendKeys("SA");
      driver.findElement(By.id("Exame_idMnmExame")).click();
      Thread.sleep(2000);
	  driver.findElement(By.id("Exame_idMnmExame")).clear();
      driver.findElement(By.id("Exame_idMnmExame")).sendKeys("TSH");
      driver.findElement(By.id("Exame_valida")).click();
      Thread.sleep(3000);	  
      driver.findElement(By.id("Exame_salva")).click();
            
      driver.findElement(By.id("lnkDiversos")).click();
      Thread.sleep(10000);    
      driver.findElement(By.id("Diversos_medicamentos")).click();
      driver.findElement(By.id("Diversos_medicamentos")).clear();
      page.setValDivMedicamentos(Medicamento);
      driver.findElement(By.id("Diversos_numeroChamada")).click();
      driver.findElement(By.id("Diversos_numeroChamada")).clear();
      page.setValNumChamada(NumChamada);
      driver.findElement(By.id("Diversos_observacoes_tecnicas")).click();
      driver.findElement(By.id("Diversos_observacoes_tecnicas")).clear();
      page.setObsTecnicas(ObsTecnicas);
      driver.findElement(By.id("Diversos_observacoes_atendimento")).clear();
      page.setObsAtd(ObsAtd);
      driver.findElement(By.id("Diversos_indicacaoClinica")).click();
      driver.findElement(By.id("Diversos_indicacaoClinica")).clear();
      page.setIndClinica(IndClinica);
      Thread.sleep(3000);
      driver.findElement(By.id("idTrouxeExAntN")).click();
      driver.findElement(By.id("Diversos_prioridade")).click();
      Select prioridade = new Select (driver.findElement(By.id("Diversos_prioridade")));
      prioridade.selectByVisibleText("N�o");
      driver.findElement(By.id("Diversos_ultimaRefeicao")).click();
      Select refeicao = new Select (driver.findElement(By.id("Diversos_ultimaRefeicao")));
      refeicao.selectByVisibleText("Desconhecido");
      
      driver.findElement(By.id("lnkRecipientes")).click();
      Thread.sleep(3000);
      String VerNumRecExames = driver.findElement(By.xpath("//*[@id=\"tabRec\"]/tbody/tr[2]/td[1]")).getText();
      Thread.sleep(3000);
      Assert.assertEquals(true, VerNumRecExames.startsWith("5"));
      Thread.sleep(3000);    
      
      driver.findElement(By.id("lnkConclusao")).click();
      Thread.sleep(10000);    
      driver.findElement(By.id("Conclusao_botConfirma")).click();
      Thread.sleep(10000);
      driver.switchTo().alert().accept();
      Thread.sleep(20000);
      driver.switchTo().alert().accept();
      Thread.sleep(10000);
      
      String mainWindow=driver.getWindowHandle();
      Set<String> set =driver.getWindowHandles();
      Iterator<String> itr= set.iterator();
      while(itr.hasNext()){
      String childWindow=itr.next();
          if(!mainWindow.equals(childWindow)){
              driver.switchTo().window(childWindow);
              System.out.println(driver.switchTo().window(childWindow).getTitle());
              driver.close();
          }
      }
      driver.switchTo().window(mainWindow);
      //driver.close();
	  
	  System.out.println("Finalizando Execu��o AdmissaoGlieseXMotion_03_TSH");	  	  		  	  	  	  
	  
  }
  
  //@Ignore
  @Test
  public void AdmissaoGlieseXMotion_04_T4L() throws Exception {
	  
	  System.out.println("Iniciando Execu��o AdmissaoGlieseXMotion_04_T4L");	  	  		  	  	  	  	  
	  
      driver.get(baseUrl);
      driver.manage().window().maximize();
      driver.findElement(By.id("username")).click();
      driver.findElement(By.id("username")).clear();
      page.setUsuario(User);
      driver.findElement(By.name("password")).clear();
      page.setSenha(Passwd);
      driver.findElement(By.id("linkEntrar")).click();

      Thread.sleep(3000);
      
      driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Selecione a Empresa'])[1]/following::div[4]")).click();
      driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Selecione a Empresa'])[1]/following::a[13]")).click();
      driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Selecione a Empresa'])[1]/following::a[13]")).click();
      
      Thread.sleep(3000);
      
      driver.findElement(By.id("resCpUnidade")).click();
      new Select(driver.findElement(By.id("resCpUnidade"))).selectByVisibleText("LVI - Vila Matilde");
      driver.findElement(By.xpath("/html/body/div[7]/div/div/div[2]/h2[1]")).click();
      driver.findElement(By.id("btEscolherEmpresa")).click();
      
      Actions action = new Actions(driver);
      WebElement we = driver.findElement(By.cssSelector("#idMenu > li:nth-child(3) > a:nth-child(1)"));
      action.moveToElement(we).build().perform();
      driver.findElement(By.xpath("//*[@id=\"mnuAtdAdm\"]")).click();
      
      Actions action1 = new Actions(driver);
      WebElement we1 = driver.findElement(By.cssSelector("#idMenu > li:nth-child(14) > a:nth-child(1)"));
      action1.moveToElement(we1).build().perform();

      Thread.sleep(5000);
      
      driver.findElement(By.linkText("Cadastro")).click();
      
      Thread.sleep(10000);
   
      try
      {
        WebElement button=driver.findElement(By.xpath("//*[@id=\"iframeAdmissao\"]"));
        driver.switchTo().frame(button);
      }
        catch (NoSuchFrameException e)
      {
        System.out.println(e.getMessage());
      }
      
      page.setDtNasc(DtNasc);      
      page.setCIP(CIP);
    
      driver.findElement(By.id("lnkVisita")).click();
      Thread.sleep(5000);
      driver.findElement(By.id("Visita_botNova")).click();
      
      driver.findElement(By.id("lnkMedico")).click();
      Thread.sleep(5000);
      page.setCRM(Medico);
      Thread.sleep(5000);
      
      driver.findElement(By.id("lnkConvenio")).click();
      Thread.sleep(5000);
      page.setConvenio(Convenio);
      driver.findElement(By.id("Convenio_valida")).click();
      Thread.sleep(5000);
      page.setMatricula(Associado);
      
      page.setDtSolicitacao(DtSolicitacao);
      driver.findElement(By.id("Convenio_planoCv")).click();
      Select planoconv = new Select (driver.findElement(By.id("Convenio_planoCv")));
      planoconv.selectByVisibleText("103 - Amil Global Jr");
      page.setValCartao(ValCartao);
      page.setValPedido(ValPedido);

      driver.findElement(By.id("lnkExames")).click();
      Thread.sleep(5000);        
      driver.findElement(By.id("Exame_idMnmMat")).click();
      driver.findElement(By.id("Exame_idMnmMat")).clear();
      driver.findElement(By.id("Exame_idMnmMat")).sendKeys("SA");
      driver.findElement(By.id("Exame_idMnmExame")).click();
      driver.findElement(By.id("Exame_idMnmExame")).clear();
      driver.findElement(By.id("Exame_idMnmExame")).sendKeys("T4L");
      driver.findElement(By.id("Exame_valida")).click();
      Thread.sleep(5000);  
      driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='TUSS'])[1]/following::img[1]")).click();
      driver.findElement(By.id("Exame_salva")).click();
     
      driver.findElement(By.id("lnkDiversos")).click();
      Thread.sleep(10000);    
      driver.findElement(By.id("Diversos_medicamentos")).click();
      driver.findElement(By.id("Diversos_medicamentos")).clear();
      page.setValDivMedicamentos(Medicamento);
      driver.findElement(By.id("Diversos_numeroChamada")).click();
      driver.findElement(By.id("Diversos_numeroChamada")).clear();
      page.setValNumChamada(NumChamada);
      driver.findElement(By.id("Diversos_observacoes_tecnicas")).click();
      driver.findElement(By.id("Diversos_observacoes_tecnicas")).clear();
      page.setObsTecnicas(ObsTecnicas);
      driver.findElement(By.id("Diversos_observacoes_atendimento")).clear();
      page.setObsAtd(ObsAtd);
      driver.findElement(By.id("Diversos_indicacaoClinica")).click();
      driver.findElement(By.id("Diversos_indicacaoClinica")).clear();
      page.setIndClinica(IndClinica);
      Thread.sleep(3000);
      driver.findElement(By.id("idTrouxeExAntN")).click();
      driver.findElement(By.id("Diversos_prioridade")).click();
      Select prioridade = new Select (driver.findElement(By.id("Diversos_prioridade")));
      prioridade.selectByVisibleText("N�o");
      driver.findElement(By.id("Diversos_ultimaRefeicao")).click();
      Select refeicao = new Select (driver.findElement(By.id("Diversos_ultimaRefeicao")));
      refeicao.selectByVisibleText("Desconhecido");
      
      driver.findElement(By.id("lnkRecipientes")).click();
      Thread.sleep(3000);
      String VerNumRecExames = driver.findElement(By.xpath("//*[@id=\"tabRec\"]/tbody/tr[2]/td[1]")).getText();
      Thread.sleep(3000);
      Assert.assertEquals(true, VerNumRecExames.startsWith("5"));
      Thread.sleep(3000);    
      
      driver.findElement(By.id("lnkConclusao")).click();
      Thread.sleep(10000);    
      driver.findElement(By.id("Conclusao_botConfirma")).click();
      Thread.sleep(10000);
      driver.switchTo().alert().accept();
      Thread.sleep(20000);
      driver.switchTo().alert().accept();
      Thread.sleep(10000);
      
      String mainWindow=driver.getWindowHandle();
      Set<String> set =driver.getWindowHandles();
      Iterator<String> itr= set.iterator();
      while(itr.hasNext()){
      String childWindow=itr.next();
          if(!mainWindow.equals(childWindow)){
              driver.switchTo().window(childWindow);
              System.out.println(driver.switchTo().window(childWindow).getTitle());
              driver.close();
          }
      }
      driver.switchTo().window(mainWindow);
      //driver.close();
	  
	  System.out.println("Finalizando Execu��o AdmissaoGlieseXMotion_04_T4L");	  	  		  	  	  	  	  	  
  
  }
 
  @Ignore
  @Test
  public void AdmissaoGlieseXMotion_09_Rollout() throws Exception {
      driver.get(baseUrl);
      driver.manage().window().maximize();
      driver.findElement(By.id("username")).click();
      driver.findElement(By.id("username")).clear();
      page.setUsuario(User);
      driver.findElement(By.name("password")).clear();
      page.setSenha(Passwd);
      driver.findElement(By.id("linkEntrar")).click();
      Thread.sleep(5000);
      driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Selecione a Empresa'])[1]/following::div[4]")).click();
      driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Selecione a Empresa'])[1]/following::a[13]")).click();
      driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Selecione a Empresa'])[1]/following::a[13]")).click();
      driver.findElement(By.xpath("/html/body/div[7]/div/div/div[1]/div/div[2]/div/ul/li[13]/a")).click();
      driver.findElement(By.id("resCpUnidade")).click();
      new Select(driver.findElement(By.id("resCpUnidade"))).selectByVisibleText("LVI - Vila Matilde");
      driver.findElement(By.xpath("/html/body/div[7]/div/div/div[2]/h2[1]")).click();
      driver.findElement(By.id("btEscolherEmpresa")).click();
      
      Actions action = new Actions(driver);
      WebElement we = driver.findElement(By.cssSelector("#idMenu > li:nth-child(3) > a:nth-child(1)"));
      action.moveToElement(we).build().perform();
      driver.findElement(By.xpath("//*[@id=\"mnuAtdAdm\"]")).click();
      
      Actions action1 = new Actions(driver);
      WebElement we1 = driver.findElement(By.cssSelector("#idMenu > li:nth-child(14) > a:nth-child(1)"));
      action1.moveToElement(we1).build().perform();

      Thread.sleep(5000);
      
      driver.findElement(By.linkText("Cadastro")).click();
      
      Thread.sleep(10000);
   
      try
      {
        WebElement button=driver.findElement(By.xpath("//*[@id=\"iframeAdmissao\"]"));
        driver.switchTo().frame(button);
      }
        catch (NoSuchFrameException e)
      {
        System.out.println(e.getMessage());
      }

      Thread.sleep(3000);
      page.setDtNasc(DtNasc);      
      Thread.sleep(3000);
      page.setCIP(CIP);
      Thread.sleep(3000);
      driver.findElement(By.id("lnkVisita")).click();
      Thread.sleep(5000);
      driver.findElement(By.id("Visita_botNova")).click();
      
      driver.findElement(By.id("lnkMedico")).click();
      Thread.sleep(5000);
      page.setCRM(Medico);
      Thread.sleep(5000);
      driver.findElement(By.id("lnkConvenio")).click();
      Thread.sleep(5000);
      page.setConvenio(Convenio);
      driver.findElement(By.id("Convenio_valida")).click();
      Thread.sleep(5000);
      page.setMatricula(Associado);
      
      page.setDtSolicitacao(DtSolicitacao);
      driver.findElement(By.id("Convenio_planoCv")).click();
      Select planoconv = new Select (driver.findElement(By.id("Convenio_planoCv")));
      planoconv.selectByVisibleText("103 - Amil Global Jr");
      page.setValCartao(ValCartao);
      page.setValPedido(ValPedido);

      driver.findElement(By.id("lnkExames")).click();
      Thread.sleep(5000);        
      driver.findElement(By.id("Exame_idMnmMat")).click();
      driver.findElement(By.id("Exame_idMnmMat")).clear();
      driver.findElement(By.id("Exame_idMnmMat")).sendKeys("SA");
      driver.findElement(By.id("Exame_idMnmExame")).click();
      driver.findElement(By.id("Exame_idMnmExame")).clear();
      driver.findElement(By.id("Exame_idMnmExame")).sendKeys("GLI");
      driver.findElement(By.id("Exame_valida")).click();
      Thread.sleep(10000);  
      driver.findElement(By.id("Exame_salva")).click();
     
      driver.findElement(By.id("lnkDiversos")).click();
      Thread.sleep(10000);    
      driver.findElement(By.id("Diversos_medicamentos")).click();
      driver.findElement(By.id("Diversos_medicamentos")).clear();
      page.setValDivMedicamentos(Medicamento);
      driver.findElement(By.id("Diversos_numeroChamada")).click();
      driver.findElement(By.id("Diversos_numeroChamada")).clear();
      page.setValNumChamada(NumChamada);
      driver.findElement(By.id("Diversos_observacoes_tecnicas")).click();
      driver.findElement(By.id("Diversos_observacoes_tecnicas")).clear();
      page.setObsTecnicas(ObsTecnicas);
      driver.findElement(By.id("Diversos_observacoes_atendimento")).clear();
      page.setObsAtd(ObsAtd);
      driver.findElement(By.id("Diversos_indicacaoClinica")).click();
      driver.findElement(By.id("Diversos_indicacaoClinica")).clear();
      page.setIndClinica(IndClinica);
      Thread.sleep(3000);
      driver.findElement(By.id("idTrouxeExAntN")).click();
      driver.findElement(By.id("Diversos_prioridade")).click();
      Select prioridade = new Select (driver.findElement(By.id("Diversos_prioridade")));
      prioridade.selectByVisibleText("N�o");
      driver.findElement(By.id("Diversos_ultimaRefeicao")).click();
      Select refeicao = new Select (driver.findElement(By.id("Diversos_ultimaRefeicao")));
      refeicao.selectByVisibleText("Desconhecido");
      
      driver.findElement(By.id("lnkRecipientes")).click();
      String VerRecExames = driver.findElement(By.xpath("//*[@id=\"tabRec\"]/tbody/tr[2]/td[6]")).getText();
      String VerNumRecExames = driver.findElement(By.xpath("//*[@id=\"tabRec\"]/tbody/tr[2]/td[1]")).getText();
      Assert.assertEquals(true, VerRecExames.contains("DA-GLISA"));
      //Assert.assertEquals(true, VerNumRecExames.startsWith("5"));

      Thread.sleep(10000);    
      
      driver.findElement(By.id("lnkConclusao")).click();
      Thread.sleep(10000);    
      driver.findElement(By.id("Conclusao_botConfirma")).click();
      Thread.sleep(10000);
      driver.switchTo().alert().accept();
      Thread.sleep(20000);
      driver.switchTo().alert().accept();
      Thread.sleep(10000);
      
      String mainWindow=driver.getWindowHandle();
      Set<String> set =driver.getWindowHandles();
      Iterator<String> itr= set.iterator();
      while(itr.hasNext()){
      String childWindow=itr.next();
          if(!mainWindow.equals(childWindow)){
              driver.switchTo().window(childWindow);
              System.out.println(driver.switchTo().window(childWindow).getTitle());
              driver.close();
          }
      }
      driver.switchTo().window(mainWindow);
      //driver.close();      
  }
  
  @Ignore
  @Test
  public void AdmissaoGlieseXMotion_10_Massive() throws Exception {
	  
	  System.out.println("Iniciando Execu��o AdmissaoGlieseXMotion_10_Massive");	  	  		  	  	  	  	  
	  
      driver.get(baseUrl);
      driver.manage().window().maximize();
      driver.findElement(By.id("username")).click();
      driver.findElement(By.id("username")).clear();
      page.setUsuario(User);
      driver.findElement(By.name("password")).clear();
      page.setSenha(Passwd);
      driver.findElement(By.id("linkEntrar")).click();
      Thread.sleep(5000);
      driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Selecione a Empresa'])[1]/following::div[4]")).click();
      driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Selecione a Empresa'])[1]/following::a[13]")).click();
      driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Selecione a Empresa'])[1]/following::a[13]")).click();
      driver.findElement(By.id("resCpUnidade")).click();
      new Select(driver.findElement(By.id("resCpUnidade"))).selectByVisibleText("LVI - Vila Matilde");
      driver.findElement(By.xpath("/html/body/div[7]/div/div/div[2]/h2[1]")).click();
      driver.findElement(By.id("btEscolherEmpresa")).click();
      
      Actions action = new Actions(driver);
      WebElement we = driver.findElement(By.cssSelector("#idMenu > li:nth-child(3) > a:nth-child(1)"));
      action.moveToElement(we).build().perform();
      driver.findElement(By.xpath("//*[@id=\"mnuAtdAdm\"]")).click();
      
      Actions action1 = new Actions(driver);
      WebElement we1 = driver.findElement(By.cssSelector("#idMenu > li:nth-child(14) > a:nth-child(1)"));
      action1.moveToElement(we1).build().perform();

      Thread.sleep(5000);
      
      driver.findElement(By.linkText("Cadastro")).click();
      
      Thread.sleep(10000);
   
      try
      {
        WebElement button=driver.findElement(By.xpath("//*[@id=\"iframeAdmissao\"]"));
        driver.switchTo().frame(button);
      }
        catch (NoSuchFrameException e)
      {
        System.out.println(e.getMessage());
      }
      
      page.setDtNasc(DtNasc);      
      page.setCIP(CIP);
    
      driver.findElement(By.id("lnkVisita")).click();
      Thread.sleep(10000);
      driver.findElement(By.id("Visita_botNova")).click();
      
      driver.findElement(By.id("lnkMedico")).click();
      Thread.sleep(10000);
      page.setCRM(Medico);
      Thread.sleep(5000);
      driver.findElement(By.id("lnkConvenio")).click();
      Thread.sleep(5000);
      page.setConvenio(Convenio);
      driver.findElement(By.id("Convenio_valida")).click();
      Thread.sleep(10000);
      page.setMatricula(Associado);
     
      page.setDtSolicitacao(DtSolicitacao);
      driver.findElement(By.id("Convenio_planoCv")).click();
      Select planoconv = new Select (driver.findElement(By.id("Convenio_planoCv")));
      planoconv.selectByVisibleText("103 - Amil Global Jr");
      page.setValCartao(ValCartao);
      page.setValPedido(ValPedido);
      
      driver.findElement(By.id("lnkExames")).click();
      Thread.sleep(5000);

      //windows
      //FileInputStream fis = new FileInputStream(System.getProperty("user.dir") + "\\MassaDados\\gxm_massadados.xls");
      //System.out.print(fis);	  

      //linux
      FileInputStream fis = new FileInputStream(System.getProperty("user.dir") + "//MassaDados//gxm_massadados.xls");
      System.out.print(fis);
	  
      Workbook wb = WorkbookFactory.create(fis);
      Sheet sheet = wb.getSheet("GXM");
      //Row row = sheet.getRow(1);
      //Cell cell = row.getCell(11);
      //System.out.println(cell.getStringCellValue());
      int i = 1;
      int j = 12;
      
      do
      {
          Row row = sheet.getRow(i++);
          Cell cell = row.getCell(j);
          String Dado1 = cell.getStringCellValue();
          //int foo = Integer.parseInt(DadoStr);
          //String DadoNum = Integer.toString(foo);
          System.out.println(Dado1);
          driver.findElement(By.id("Exame_idMnmMat")).click();
          driver.findElement(By.id("Exame_idMnmMat")).clear();
          driver.findElement(By.id("Exame_idMnmMat")).sendKeys("SA");
          driver.findElement(By.id("Exame_idMnmExame")).click();
          driver.findElement(By.id("Exame_idMnmExame")).clear();
          driver.findElement(By.id("Exame_idMnmExame")).sendKeys(Dado1);
          driver.findElement(By.id("Exame_valida")).click();

          try {
              driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='TUSS'])[1]/following::img[1]")).click();
              driver.findElement(By.id("Exame_salva")).click();
              Thread.sleep(3000);
          }
          catch (Exception ex)
          {
              driver.findElement(By.id("Exame_salva")).click();
              Thread.sleep(3000);
          }
          
          driver.findElement(By.id("Exame_botNovo")).click();
          Thread.sleep(3000);
          
          /*driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='TUSS'])[1]/following::img[1]")).click();
          
          driver.findElement(By.id("Exame_salva")).click();
          Thread.sleep(5000);
          driver.findElement(By.id("Exame_botNovo")).click();
          Thread.sleep(5000);*/
      }
      while(i<10);
                  
      driver.findElement(By.id("lnkDiversos")).click();
      Thread.sleep(10000);    
      driver.findElement(By.id("Diversos_medicamentos")).click();
      driver.findElement(By.id("Diversos_medicamentos")).clear();
      page.setValDivMedicamentos(Medicamento);
	  
      driver.findElement(By.id("Diversos_ultimaMenstruacao")).click();
      driver.findElement(By.id("Diversos_ultimaMenstruacao")).clear();
      driver.findElement(By.id("Diversos_ultimaMenstruacao")).sendKeys("01012018");
	  
      driver.findElement(By.id("Diversos_numeroChamada")).click();
      driver.findElement(By.id("Diversos_numeroChamada")).clear();
      page.setValNumChamada(NumChamada);
      driver.findElement(By.id("Diversos_observacoes_tecnicas")).click();
      driver.findElement(By.id("Diversos_observacoes_tecnicas")).clear();
      page.setObsTecnicas(ObsTecnicas);
      driver.findElement(By.id("Diversos_observacoes_atendimento")).clear();
      page.setObsAtd(ObsAtd);
      driver.findElement(By.id("Diversos_indicacaoClinica")).click();
      driver.findElement(By.id("Diversos_indicacaoClinica")).clear();
      page.setIndClinica(IndClinica);
      Thread.sleep(3000);
      driver.findElement(By.id("idTrouxeExAntN")).click();
      driver.findElement(By.id("Diversos_prioridade")).click();
      Select prioridade = new Select (driver.findElement(By.id("Diversos_prioridade")));
      prioridade.selectByVisibleText("N�o");
      driver.findElement(By.id("Diversos_ultimaRefeicao")).click();
      Select refeicao = new Select (driver.findElement(By.id("Diversos_ultimaRefeicao")));
      refeicao.selectByVisibleText("Desconhecido");
      
      driver.findElement(By.id("lnkRecipientes")).click();
      Thread.sleep(10000);    
      String VerRecExames = driver.findElement(By.xpath("//*[@id=\"tabRec\"]/tbody/tr[2]/td[6]")).getText();
      String VerNumRecExames = driver.findElement(By.xpath("//*[@id=\"tabRec\"]/tbody/tr[2]/td[1]")).getText();
      Assert.assertEquals(true, VerRecExames.contains("DA-GLISA"));
      //Assert.assertEquals(true, VerNumRecExames.startsWith("5"));
      
      driver.findElement(By.id("lnkConclusao")).click();
      Thread.sleep(10000);    
      driver.findElement(By.id("Conclusao_botConfirma")).click();
      Thread.sleep(10000);
      driver.switchTo().alert().accept();
      Thread.sleep(20000);
      driver.switchTo().alert().accept();
      Thread.sleep(10000);
      
      String mainWindow=driver.getWindowHandle();
      Set<String> set =driver.getWindowHandles();
      Iterator<String> itr= set.iterator();
      while(itr.hasNext()){
      String childWindow=itr.next();
          if(!mainWindow.equals(childWindow)){
              driver.switchTo().window(childWindow);
              System.out.println(driver.switchTo().window(childWindow).getTitle());
              driver.close();
          }
      }
      driver.switchTo().window(mainWindow);
      //driver.close();
	  
	  System.out.println("Finalizando Execu��o AdmissaoGlieseXMotion_10_Massive");	  	  		  	  	  	  	  
  }
  
  //@Ignore
  @Test
  public void AdmissaoGlieseXMotion_06_Urgencia() throws Exception {
	  
	  System.out.println("Iniciando Execu��o AdmissaoGlieseXMotion_06_Urgencia");	  	  		  	  	  	  	  	  
	  
      driver.get(baseUrl);
      driver.manage().window().maximize();
      driver.findElement(By.id("username")).click();
      driver.findElement(By.id("username")).clear();
      page.setUsuario(User);
      driver.findElement(By.name("password")).clear();
      page.setSenha(Passwd);
      driver.findElement(By.id("linkEntrar")).click();
      Thread.sleep(5000);
      driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Selecione a Empresa'])[1]/following::div[4]")).click();
      driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Selecione a Empresa'])[1]/following::a[13]")).click();
      driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Selecione a Empresa'])[1]/following::a[13]")).click();
      driver.findElement(By.id("resCpUnidade")).click();
      new Select(driver.findElement(By.id("resCpUnidade"))).selectByVisibleText("LVI - Vila Matilde");
      driver.findElement(By.xpath("/html/body/div[7]/div/div/div[2]/h2[1]")).click();
      driver.findElement(By.id("btEscolherEmpresa")).click();
      
      Actions action = new Actions(driver);
      WebElement we = driver.findElement(By.cssSelector("#idMenu > li:nth-child(3) > a:nth-child(1)"));
      action.moveToElement(we).build().perform();
      driver.findElement(By.xpath("//*[@id=\"mnuAtdAdm\"]")).click();
      
      Actions action1 = new Actions(driver);
      WebElement we1 = driver.findElement(By.cssSelector("#idMenu > li:nth-child(14) > a:nth-child(1)"));
      action1.moveToElement(we1).build().perform();

      Thread.sleep(5000);
      
      driver.findElement(By.linkText("Cadastro")).click();
      
      Thread.sleep(10000);
   
      try
      {
        WebElement button=driver.findElement(By.xpath("//*[@id=\"iframeAdmissao\"]"));
        driver.switchTo().frame(button);
      }
        catch (NoSuchFrameException e)
      {
        System.out.println(e.getMessage());
      }
      
      page.setDtNasc(DtNasc);
      page.setCIP(CIP);
    
      driver.findElement(By.id("lnkVisita")).click();
      Thread.sleep(10000);
      driver.findElement(By.id("Visita_botNova")).click();
      Thread.sleep(10000);
      
      driver.findElement(By.id("lnkMedico")).click();
      Thread.sleep(5000);
      page.setCRM(Medico);
      Thread.sleep(5000);
      
      driver.findElement(By.id("lnkConvenio")).click();
      Thread.sleep(5000);
      page.setConvenio(Convenio);
      driver.findElement(By.id("Convenio_valida")).click();
      Thread.sleep(10000);
      page.setMatricula(Associado);
     
      page.setDtSolicitacao(DtSolicitacao);
      driver.findElement(By.id("Convenio_planoCv")).click();
      Select planoconv = new Select (driver.findElement(By.id("Convenio_planoCv")));
      planoconv.selectByVisibleText("103 - Amil Global Jr");
      page.setValCartao(ValCartao);
      page.setValPedido(ValPedido);
      
      driver.findElement(By.id("lnkExames")).click();
      Thread.sleep(5000);        
      driver.findElement(By.id("Exame_idMnmMat")).click();
      driver.findElement(By.id("Exame_idMnmMat")).clear();
      driver.findElement(By.id("Exame_idMnmMat")).sendKeys("SA");
      driver.findElement(By.id("Exame_idMnmExame")).click();
      driver.findElement(By.id("Exame_idMnmExame")).clear();
      driver.findElement(By.id("Exame_idMnmExame")).sendKeys("GLI");
      driver.findElement(By.id("Exame_valida")).click();
      driver.findElement(By.id("Exame_salva")).click();
            
      driver.findElement(By.id("lnkDiversos")).click();
      Thread.sleep(10000);    
      driver.findElement(By.id("Diversos_medicamentos")).click();
      driver.findElement(By.id("Diversos_medicamentos")).clear();
      page.setValDivMedicamentos(Medicamento);
      driver.findElement(By.id("Diversos_numeroChamada")).click();
      driver.findElement(By.id("Diversos_numeroChamada")).clear();
      page.setValNumChamada(NumChamada);
      driver.findElement(By.id("Diversos_observacoes_tecnicas")).click();
      driver.findElement(By.id("Diversos_observacoes_tecnicas")).clear();
      page.setObsTecnicas(ObsTecnicas);
      driver.findElement(By.id("Diversos_observacoes_atendimento")).clear();
      page.setObsAtd(ObsAtd);
      driver.findElement(By.id("Diversos_indicacaoClinica")).click();
      driver.findElement(By.id("Diversos_indicacaoClinica")).clear();
      page.setIndClinica(IndClinica);
      Thread.sleep(3000);
      driver.findElement(By.id("idTrouxeExAntN")).click();
      driver.findElement(By.id("Diversos_prioridade")).click();
      Select prioridade = new Select (driver.findElement(By.id("Diversos_prioridade")));
      prioridade.selectByVisibleText("Sim");
      driver.findElement(By.id("Diversos_ultimaRefeicao")).click();
      Select refeicao = new Select (driver.findElement(By.id("Diversos_ultimaRefeicao")));
      refeicao.selectByVisibleText("Desconhecido");
      
      driver.findElement(By.id("lnkRecipientes")).click();
      Thread.sleep(10000);    
      String VerRecExames = driver.findElement(By.xpath("//*[@id=\"tabRec\"]/tbody/tr[2]/td[6]")).getText();
      String VerNumRecExames = driver.findElement(By.xpath("//*[@id=\"tabRec\"]/tbody/tr[2]/td[1]")).getText();
      Assert.assertEquals(true, VerRecExames.contains("DA-GLISA"));
      Assert.assertEquals(true, VerNumRecExames.startsWith("5"));
      
      driver.findElement(By.id("lnkConclusao")).click();
      Thread.sleep(10000);    
      driver.findElement(By.id("Conclusao_botConfirma")).click();
      Thread.sleep(10000);
      driver.switchTo().alert().accept();
      Thread.sleep(20000);
      driver.switchTo().alert().accept();
      Thread.sleep(10000);
      
      String mainWindow=driver.getWindowHandle();
      Set<String> set =driver.getWindowHandles();
      Iterator<String> itr= set.iterator();
      while(itr.hasNext()){
      String childWindow=itr.next();
          if(!mainWindow.equals(childWindow)){
              driver.switchTo().window(childWindow);
              System.out.println(driver.switchTo().window(childWindow).getTitle());
              driver.close();
          }
      }
      driver.switchTo().window(mainWindow);
      //driver.close();

	  System.out.println("Finalizando Execu��o AdmissaoGlieseXMotion_06_Urgencia");	  	  		  	  	  	  	  	  	  
	  
  }
  
  @Ignore     
  @Test
  public void AdmissaoGlieseXMotion_07_AtendFila() throws Exception {
	  
	  System.out.println("Iniciando Execu��o AdmissaoGlieseXMotion_07_AtendFila");	  	  		  	  	  	  	  	  	  
	  
      driver.get(baseUrl);
      driver.manage().window().maximize();
      driver.findElement(By.id("username")).click();
      driver.findElement(By.id("username")).clear();
      page.setUsuario(User);
      driver.findElement(By.name("password")).clear();
      page.setSenha(Passwd);
      driver.findElement(By.id("linkEntrar")).click();
      Thread.sleep(5000);
      driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Selecione a Empresa'])[1]/following::div[4]")).click();
      driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Selecione a Empresa'])[1]/following::a[13]")).click();
      driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Selecione a Empresa'])[1]/following::a[13]")).click();
      driver.findElement(By.id("resCpUnidade")).click();
      new Select(driver.findElement(By.id("resCpUnidade"))).selectByVisibleText("LVI - Vila Matilde");
      driver.findElement(By.xpath("/html/body/div[7]/div/div/div[2]/h2[1]")).click();
      driver.findElement(By.id("btEscolherEmpresa")).click();
      
      driver.findElement(By.cssSelector("a#linkColeta")).click();
      Thread.sleep(3000);
      driver.findElement(By.cssSelector("a#mnuColFila")).click();
      Thread.sleep(3000);
      
      driver.switchTo().frame("iframeAdmissao");
      Actions action1 = new Actions(driver);
      WebElement we1 = driver.findElement(By.cssSelector("div#impressoraDiv"));
      action1.moveToElement(we1).build().perform();
      Thread.sleep(5000);
      driver.findElement(By.cssSelector("div#impressoraDiv > input[type=\"button\"]")).click();
      Thread.sleep(5000);
      
      driver.findElement(By.cssSelector("ul#listaInicio li:nth-child(1) > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > input:nth-child(5)")).click();
      Thread.sleep(5000);

      driver.findElement(By.cssSelector("ul#listaInicio li:nth-child(1) > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > input:nth-child(4)")).click();
      Thread.sleep(5000);

      driver.findElement(By.cssSelector("ul#listaPrioridade li:nth-child(1) > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > input:nth-child(4)")).click();
      Thread.sleep(5000);
      
      driver.findElement(By.cssSelector("ul#listaAtendimento li:nth-child(1) > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > input.botaoCronometro")).click();
      Thread.sleep(5000);
      driver.switchTo().alert().accept();
      Thread.sleep(10000);
	  
 	  System.out.println("Iniciando Execu��o AdmissaoGlieseXMotion_07_AtendFila");	  	  		  	  	  	  	  	  	  
	  
  } 
  
  //@Ignore
  @Test
  public void AdmissaoGlieseXMotion_08_Request() throws Exception {

   	  System.out.println("Iniciando Execu��o AdmissaoGlieseXMotion_08_Request");	  	  		  	  	  	  	  	  	  
  
      /*driver.get(baseUrl);
      driver.manage().window().maximize();
      driver.findElement(By.id("username")).click();
      driver.findElement(By.id("username")).clear();
      page.setUsuario(User);
      driver.findElement(By.name("password")).clear();
      page.setSenha(Passwd);
      driver.findElement(By.id("linkEntrar")).click();
      Thread.sleep(5000);
      driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Selecione a Empresa'])[1]/following::div[4]")).click();
      driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Selecione a Empresa'])[1]/following::a[13]")).click();
      driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Selecione a Empresa'])[1]/following::a[13]")).click();
      driver.findElement(By.id("resCpUnidade")).click();
      new Select(driver.findElement(By.id("resCpUnidade"))).selectByVisibleText("LVI - Vila Matilde");
      driver.findElement(By.xpath("/html/body/div[7]/div/div/div[2]/h2[1]")).click();
      driver.findElement(By.id("btEscolherEmpresa")).click();
      
      Actions action = new Actions(driver);
      WebElement we = driver.findElement(By.cssSelector("#idMenu > li:nth-child(3) > a:nth-child(1)"));
      action.moveToElement(we).build().perform();
      driver.findElement(By.xpath("//*[@id=\"mnuAtdAdm\"]")).click();
      
      Actions action1 = new Actions(driver);
      WebElement we1 = driver.findElement(By.cssSelector("#idMenu > li:nth-child(14) > a:nth-child(1)"));
      action1.moveToElement(we1).build().perform();

      Thread.sleep(5000);
      
      driver.findElement(By.linkText("Cadastro")).click();
      
      Thread.sleep(10000);
   
      try
      {
        WebElement button=driver.findElement(By.xpath("//*[@id=\"iframeAdmissao\"]"));
        driver.switchTo().frame(button);
      }
        catch (NoSuchFrameException e)
      {
        System.out.println(e.getMessage());
      }
      
      page.setDtNasc(DtNasc);
      page.setCIP(CIP);
    
      driver.findElement(By.id("lnkVisita")).click();
      Thread.sleep(10000);
      driver.findElement(By.id("Visita_botNova")).click();
      
      driver.findElement(By.id("lnkMedico")).click();
      Thread.sleep(5000);
      page.setCRM(Medico);
      
      driver.findElement(By.id("lnkConvenio")).click();
      Thread.sleep(5000);
      page.setConvenio(Convenio);
      driver.findElement(By.id("Convenio_valida")).click();
      Thread.sleep(10000);
      page.setMatricula(Associado);
     
      page.setDtSolicitacao(DtSolicitacao);
      driver.findElement(By.id("Convenio_planoCv")).click();
      Select planoconv = new Select (driver.findElement(By.id("Convenio_planoCv")));
      planoconv.selectByVisibleText("103 - Amil Global Jr");
      page.setValCartao(ValCartao);
      page.setValPedido(ValPedido);
      
      driver.findElement(By.id("lnkExames")).click();
      Thread.sleep(5000);        
      driver.findElement(By.id("Exame_idMnmMat")).click();
      driver.findElement(By.id("Exame_idMnmMat")).clear();
      driver.findElement(By.id("Exame_idMnmMat")).sendKeys("SA");
      driver.findElement(By.id("Exame_idMnmExame")).click();
      driver.findElement(By.id("Exame_idMnmExame")).clear();
      driver.findElement(By.id("Exame_idMnmExame")).sendKeys("GLI");
      driver.findElement(By.id("Exame_valida")).click();
      driver.findElement(By.id("Exame_salva")).click();
            
      driver.findElement(By.id("lnkDiversos")).click();
      Thread.sleep(10000);    
      driver.findElement(By.id("Diversos_medicamentos")).click();
      driver.findElement(By.id("Diversos_medicamentos")).clear();
      page.setValDivMedicamentos(Medicamento);
      driver.findElement(By.id("Diversos_numeroChamada")).click();
      driver.findElement(By.id("Diversos_numeroChamada")).clear();
      page.setValNumChamada(NumChamada);
      driver.findElement(By.id("Diversos_observacoes_tecnicas")).click();
      driver.findElement(By.id("Diversos_observacoes_tecnicas")).clear();
      page.setObsTecnicas(ObsTecnicas);
      driver.findElement(By.id("Diversos_observacoes_atendimento")).clear();
      page.setObsAtd(ObsAtd);
      driver.findElement(By.id("Diversos_indicacaoClinica")).click();
      driver.findElement(By.id("Diversos_indicacaoClinica")).clear();
      page.setIndClinica(IndClinica);
      Thread.sleep(3000);
      driver.findElement(By.id("idTrouxeExAntN")).click();
      driver.findElement(By.id("Diversos_prioridade")).click();
      Select prioridade = new Select (driver.findElement(By.id("Diversos_prioridade")));
      prioridade.selectByVisibleText("N�o");
      driver.findElement(By.id("Diversos_ultimaRefeicao")).click();
      Select refeicao = new Select (driver.findElement(By.id("Diversos_ultimaRefeicao")));
      refeicao.selectByVisibleText("Desconhecido");
      Thread.sleep(10000);
      driver.findElement(By.id("lnkRecipientes")).click();
      Thread.sleep(10000);    
      String VerRecExames = driver.findElement(By.xpath("//*[@id=\"tabRec\"]/tbody/tr[2]/td[6]")).getText();
      String VerNumRecExames = driver.findElement(By.xpath("//*[@id=\"tabRec\"]/tbody/tr[2]/td[1]")).getText();
      Assert.assertEquals(true, VerRecExames.contains("DA-GLISA"));
      //Assert.assertEquals(true, VerNumRecExames.startsWith("5"));
      
      driver.findElement(By.id("lnkConclusao")).click();
      Thread.sleep(10000);    
      driver.findElement(By.id("Conclusao_botConfirma")).click();
      Thread.sleep(10000);
      driver.switchTo().alert().accept();
      Thread.sleep(20000);
      driver.switchTo().alert().accept();
      Thread.sleep(10000);
      
      String mainWindow=driver.getWindowHandle();
      Set<String> set =driver.getWindowHandles();
      Iterator<String> itr= set.iterator();
      while(itr.hasNext()){
      String childWindow=itr.next();
          if(!mainWindow.equals(childWindow)){
              driver.switchTo().window(childWindow);
              System.out.println(driver.switchTo().window(childWindow).getTitle());
              driver.close();
          }
      }
      driver.switchTo().window(mainWindow);
      //driver.close();*/
      
      driver.get(baseUrlMotion);
      driver.manage().window().maximize();
      driver.findElement(By.xpath("//body/div[2]/div/div[2]/div[2]/form/div/input")).click();
      driver.findElement(By.xpath("//body/div[2]/div/div[2]/div[2]/form/div/input")).clear();
      Thread.sleep(5000);
      page.setUsuarioMotion(usuarioMotion);
      driver.findElement(By.xpath("//body/div[2]/div/div[2]/div[2]/form/div[2]/input")).clear();
      Thread.sleep(5000);
      page.setSenhaMotion(senhaMotion);
      Thread.sleep(5000);
      driver.findElement(By.cssSelector("div#login-button")).click();
      Thread.sleep(5000);
	  
   	  System.out.println("Finalizando Execu��o AdmissaoGlieseXMotion_08_Request");	  	  		  	  	  	  	  	  	  	  
	  
  }
}