import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AdmissaoGlieseXMotionPage {
    private DSL dsl;
    
    //para o Gliese	
    public AdmissaoGlieseXMotionPage(WebDriver driver) {
        dsl = new DSL(driver);
    }
    
    public void setUsuario(String texto) {
        dsl.escreveUsuario("username", texto);        
    }
    
    public void setSenha(String texto) {
        dsl.escreveSenha("password", texto);        
    }
    
    public void setDtNasc(String texto) {
        dsl.escreveDtNasc("Paciente_txtDtNasc", texto);
    }    
    
    public void setCIP(String texto) {
        dsl.escreveCIP("Paciente_txtCip", texto);
    }
    
    public void setCIPLock(String texto) {
        dsl.escreveCIP("lockCpCIP", texto);
    }
    
    public void setCRM(String texto) {
        dsl.escreveCRM("txtCrm", texto);
    }
    
    public void setConvenio(String texto) {
        dsl.escreveCRM("Convenio_convenio", texto);
    }
        
    public void setMatricula(String texto) {
        dsl.escreveMatricula("Convenio_matricAssociado", texto);
    }
    
    public void setDtSolicitacao(String texto) {
        dsl.escreveDtSolicitacao("Convenio_dataSolicitacao", texto);
    }
    
    public void setValCartao(String texto) {
        dsl.escreveValCartao("Convenio_valCartao", texto);
    }
    
    public void setValPedido(String texto) {
        dsl.escreveValPedido("Convenio_valPedido", texto);
    }
    
    public void setValDivMedicamentos(String texto) {
        dsl.escreveValPedido("Diversos_medicamentos", texto);
    }
    
    public void setValNumChamada(String texto) {
        dsl.escreveValPedido("Diversos_medicamentos", texto);
    }
    
    public void setObsTecnicas(String texto) {
        dsl.escreveObsTecnicas("Diversos_observacoes_tecnicas", texto);
    }
    
    public void setObsAtd(String texto) {
        dsl.escreveObsAtd("Diversos_observacoes_atendimento", texto);
    }
    
    public void setIndClinica(String texto) {
        dsl.escreveIndClinica("Diversos_indicacaoClinica", texto);
    }
    
    //para o Motion
    public void setUsuarioMotion(String texto) {
        dsl.escreveUsuarioMotion("//body/div[2]/div/div[2]/div[2]/form/div/input", texto);        
    } 
    
    public void setSenhaMotion(String texto) {
        dsl.escreveSenhaMotion("//body/div[2]/div/div[2]/div[2]/form/div[2]/input", texto);        
    }

	
}