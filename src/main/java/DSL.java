import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DSL {
    private WebDriver driver;
    
    public DSL (WebDriver driver) {
        this.driver = driver;
    }
    
    //M�todos para escrever dados, ex: nome, crm, cip etc) no Gliese
    public void escreveUsuario(String id_campo, String texto) {
        driver.findElement(By.id(id_campo)).sendKeys(texto);
    }
    
    public void escreveSenha(String password, String texto) {
        driver.findElement(By.name(password)).sendKeys(texto);        
    }   
    
    public void escreveDtNasc(String Paciente_txtDtNasc, String texto) {
        driver.findElement(By.id(Paciente_txtDtNasc)).sendKeys(texto);        
    }    
    
    public void escreveCIP(String Paciente_txtCip, String texto) {
        driver.findElement(By.id(Paciente_txtCip)).sendKeys(texto);        
    }
    
    public void escreveCIPLock(String lockCpCIP, String texto) {
        driver.findElement(By.id(lockCpCIP)).sendKeys(texto);        
    }
    
    public void escreveCRM(String txtCrm, String texto) {
        driver.findElement(By.id(txtCrm)).sendKeys(texto);        
    }
    
    public void escreveConvenio(String Convenio_convenio, String texto) {
        driver.findElement(By.id(Convenio_convenio)).sendKeys(texto);
    }
    
    public void escreveMatricula(String Convenio_matricAssociado, String texto) {
        driver.findElement(By.id(Convenio_matricAssociado)).sendKeys(texto);
    }
    
    public void escreveDtSolicitacao(String Convenio_dataSolicitacao, String texto) {
        driver.findElement(By.id(Convenio_dataSolicitacao)).sendKeys(texto);
    } 
    
    public void escreveValCartao(String Convenio_valCartao, String texto) {
        driver.findElement(By.id(Convenio_valCartao)).sendKeys(texto);
    }
    
    public void escreveValPedido(String Convenio_valPedido, String texto) { 
        driver.findElement(By.id(Convenio_valPedido)).sendKeys(texto);
    }
    
    public void escreveValDivMedicamentos(String Diversos_medicamentos, String texto) { 
        driver.findElement(By.id(Diversos_medicamentos)).sendKeys(texto);
    }
    
    public void escreveValNumChamada(String Diversos_numeroChamada, String texto) { 
        driver.findElement(By.id(Diversos_numeroChamada)).sendKeys(texto);
    }
    
    public void escreveObsTecnicas(String Diversos_observacoes_tecnicas, String texto) { 
        driver.findElement(By.id(Diversos_observacoes_tecnicas)).sendKeys(texto);
    }
    
    public void escreveObsAtd(String Diversos_observacoes_atendimento, String texto) { 
        driver.findElement(By.id(Diversos_observacoes_atendimento)).sendKeys(texto);
    }
    
    public void escreveIndClinica(String Diversos_indicacaoClinica, String texto) { 
        driver.findElement(By.id(Diversos_indicacaoClinica)).sendKeys(texto);
    }    
    
    //M�todos para escrever dados, ex: nome, crm, cip etc) no Motion
    public void escreveUsuarioMotion(String userMotion, String texto) {
        driver.findElement(By.xpath(userMotion)).sendKeys(texto);
    }
    
    public void escreveSenhaMotion(String passwordMotion, String texto) {
        driver.findElement(By.xpath(passwordMotion)).sendKeys(texto);        
    }

}