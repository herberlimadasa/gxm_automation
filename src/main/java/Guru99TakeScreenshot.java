import java.io.File;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
public class Guru99TakeScreenshot {

    @Test
    public void testGuru99TakeScreenShot() throws Exception{
        WebDriver driver;
        System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "\\Utils\\Automation\\geckodriver\\geckodriver.exe");
        driver = new FirefoxDriver();
        //goto url
        driver.get("http://10.120.42.1:7003/gliese-gxm-hml/loginPortalLSF.jsp");
        //Call take screenshot function
        this.takeSnapShot(driver, "C:\\Users\\Herbert\\eclipse-workspace\\GlisexMotion\\ScreenShot\\test.png");
    }

    /**
     * This function will take screenshot
     * @param webdriver
     * @param fileWithPath
     * @throws Exception
     */
    public static void takeSnapShot(WebDriver webdriver,String fileWithPath) throws Exception{
        //Convert web driver object to TakeScreenshot
        TakesScreenshot scrShot =((TakesScreenshot)webdriver);
        //Call getScreenshotAs method to create image file
        File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
        //Move image file to new destination
        File DestFile=new File(fileWithPath);
        //Copy file at destination
        FileUtils.copyFile(SrcFile, DestFile);
    }
}